package com.example.student1.thirdproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
TextView text;
    //EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        text=(TextView) findViewById(R.id.text);
        Intent intent=getIntent();
        String username=intent.getExtras().getString("login");
        String password=intent.getExtras().getString("password");

        text.setText("Здравствуй"+" "+username + "\n" + "Твой пароль: " + password);

    }
}
