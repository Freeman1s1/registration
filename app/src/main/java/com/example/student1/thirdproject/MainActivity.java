package com.example.student1.thirdproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView login;
    TextView password;
    TextView password1;
    TextView rememberText;
    EditText e_login;
    EditText e_password;
    EditText e_password1;
    Button button;
    CheckBox remember;
    SharedPreferences sPref;
    final String SAVE_LOGIN="save_login";
    final String SAVE_PASSWORD="save_password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = (TextView) findViewById(R.id.login);
        password = (TextView) findViewById(R.id.password);
        password1 = (TextView) findViewById(R.id.password1);
        rememberText=(TextView) findViewById(R.id.remember);
        e_login = (EditText) findViewById(R.id.e_login);
        e_password = (EditText) findViewById(R.id.e_password);
        e_password1 = (EditText) findViewById(R.id.e_password1);
        remember=(CheckBox) findViewById(R.id.check_remember);
        button = (Button) findViewById(R.id.button);

        button.setText("Зарегестрироваться");
        button.setOnClickListener(this);

            sPref = getPreferences(MODE_PRIVATE);
            String saveLogin = sPref.getString(SAVE_LOGIN, "");
            String savePassword = sPref.getString(SAVE_PASSWORD, "");
            e_login.setText(saveLogin);
            e_password.setText(savePassword);

    }

    @Override
    public void onClick(View view) {
        Validator validator=new Validator();
       String username=e_login.getText().toString();
        String difficult=validator.checkPasswordOnDifficulty(e_password);
        Intent intent = new Intent(this, Main2Activity.class);
intent.putExtra("login", username);
        intent.putExtra("password", difficult);
        if(remember.isChecked()){
          sPref=getPreferences(MODE_PRIVATE);
          SharedPreferences.Editor ed=sPref.edit();
            ed.putString(SAVE_LOGIN, e_login.getText().toString());
            ed.putString(SAVE_PASSWORD, e_password.getText().toString());
            ed.apply();

        }else{
            sPref=getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor ed=sPref.edit();
            ed.putString(SAVE_LOGIN, "Логин");
            ed.putString(SAVE_PASSWORD, "Пароль");
            ed.apply();
        }


        if(validator.checkRepeatPassword(e_password, e_password1)==true && validator.checkLogin(e_login)==true && validator.checkEasyPassword(e_password)==false){
            startActivity(intent);
        }
    }
}