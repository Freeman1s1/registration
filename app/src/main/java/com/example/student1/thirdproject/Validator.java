package com.example.student1.thirdproject;

/**
 * Created by alexander on 25.11.16.
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.regex.*;

public class Validator {

    public boolean checkRepeatPassword(EditText pass, EditText pass1){
        boolean res=false;
        String password=pass.getText().toString();
        String password1=pass1.getText().toString();
        if(password.equals(password1)){
            res=true;
        }
        return res;
    }
    public boolean checkForEmptines(String check){
        boolean res=false;
        if(check==null || check.trim().length()==0){
            res=true;
        }
        return res;
    }
    public boolean checkLogin(EditText log){
        boolean res=false;
        String login=log.getText().toString();

        if(checkForEmptines(login)==false  ){
            res=true;
        }
        return res;
    }
    public boolean checkEasyPassword(EditText pass){
        boolean res=true;
        String password=pass.getText().toString();
        String password_pattern="((?=.*\\d)(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])|(?=.*\\d)(?=.*[@#$%])|(?=.*[A-Z])(?=.*[a-z])|(?=.*[a-z])(?=.*[@#$%])|(?=.*[A-Z])(?=.*[@#$%]))";
        Pattern pattern=Pattern.compile(password_pattern);
        Matcher matcher=pattern.matcher(password);
        while(matcher.find()){
            if(password.length()>=6){
            res=false;
            }
        }
        return res;
    }
    public String checkPasswordOnDifficulty(EditText pass){
        String res="";
        String password_pattern="((?=.*\\d)(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])|(?=.*\\d)(?=.*[@#$%])|(?=.*[A-Z])(?=.*[a-z])|(?=.*[a-z])(?=.*[@#$%])|(?=.*[A-Z])(?=.*[@#$%]))";
        String password=pass.getText().toString();
        if(password.length()>=13){
            Pattern pattern=Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%])");
            Matcher matcher=pattern.matcher(password);
            while(matcher.find()){
                res="Очень сложный";
            }
        }
        if(password.length()>=10 && password.length()<=12){
            Pattern pattern=Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])|(?=.*\\d)(?=.*[a-z])(?=.*[@#$%])|(?=.*\\d)(?=.*[@#$%])(?=.*[A-Z])|(?=.*[@#$%])(?=.*[a-z])(?=.*[A-Z])");
            Matcher matcher=pattern.matcher(password);
            while(matcher.find()){
                res="Сложный";
            }
        }
        if(password.length()>=7 && password.length()<=9){
            Pattern pattern=Pattern.compile(password_pattern);
            Matcher matcher=pattern.matcher(password);
            while(matcher.find()){
                res="Средний";
            }
        }
        if(password.length()==6){
            Pattern pattern=Pattern.compile(password_pattern);
            Matcher matcher=pattern.matcher(password);
            while(matcher.find()){
                res="Лёгкий";
            }
        }
        if(password.length()<6){
           res="Очень лёгкий";
        }
        return res;
    }
}
